

var applauncher = (function() {
	
	//
	// Private properties and methods
	//
	id = 'appLauncherApplet';
	requiredVersion = '0.3.7';
	browserSupported = true;
	browser = navigator.userAgent.toLowerCase();
	
	
	var init = function() {
		var body = document.getElementsByTagName('body')[0];
		
		if (browser.indexOf('firefox') > -1 || browser.indexOf('chrome') > -1) {	    	
			var applet = '<embed code="com.gmail.ianspigeon.applauncher.AppLauncher.class" archive="applauncher.jar"' +
						 'id="' + id + '" width="1" height="1" type="application/x-java-applet;version=1.6" />';
			body.insertAdjacentHTML('afterend', applet);
		
		} else if (browser.indexOf('msie') > -1) {
			var applet = '<OBJECT classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" id="' + id + '" width="1" height="1">'+
						 '<PARAM name="code" value="com.gmail.ianspigeon.applauncher.AppLauncher.class" />'+
						 '<PARAM name="archive" value="applauncher.jar" />'+
						 '</OBJECT>';
			body.insertAdjacentHTML('afterend', applet);
			
		} else {
			applauncher.browserSupported = false;     
		}
		
	}
	
	
	var getApplet = function() {
		
		if (browserSupported) {
		    var applet = document.getElementById(id);		    
		    
		    try {
		        var version = applet.getVersion();
		        if (version != requiredVersion) {
		            alert('Incorrect version. Requires ' + requiredVersion + ', detected version ' + 
		            	  version + '. Close all browser windows and retry.');
		            return;
		        }
	
		    } catch (error) {
		        alert('Unable to determine AppLauncher version. Please make sure you have Java ' + 
		        	  'installed and enabled, then close all browser windows and retry.');
		        return;
		    }
		    
		    return applet;
		
		} else {
			alert('Unsupport browser, Internet Explorer, Firefox or Chrome required.');
		}
	}


	var checkResult = function(result) {
		
		if (result != 'SUCCESS') {
		    alert(result);
		}
		
	}
	
	init();
	
	
	//
	// Public methods
	//
	return {
		
		rdp: function(hostname) {
			var applet = getApplet();
			if (applet != null) {
				var result = applet.launchRDesktop(hostname, '3389');
				checkResult(result);
			}	
		},


		esx: function(hostname, vm) {
			var applet = getApplet();
			if (applet != null) {
				var result = applet.launchESXClient(hostname, vm);
				checkResult(result);
			}
		},


		ssh: function(hostname, username) {
			var applet = getApplet();
			if (applet != null) {
				var result = applet.launchSSH(hostname, '22', username);
				checkResult(result);
			}
		},


		sftp: function(hostname) {
			var applet = getApplet();
			if (applet != null) {
				var result = applet.launchSFTP(hostname, '22');
				checkResult(result);
			}
		},
		
		
		file: function(path) {
			var applet = getApplet();
			if (applet != null) {
				var result = applet.launchFileManager(path);
				checkResult(result);
			}
		}
	
	}	
	
}());
