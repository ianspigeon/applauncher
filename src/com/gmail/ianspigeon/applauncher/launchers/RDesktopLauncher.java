package com.gmail.ianspigeon.applauncher.launchers;



public class RDesktopLauncher extends Launcher {
	
	
	public String launch(String hostname, String port) {
		
		System.out.println("RDesktopLauncher call detected." );
		
		try {
			validateParameter("hostname", hostname, "^[\\w.\\-_]+$");
			validateParameter("port", port, "^[\\d]+$");
			
		} catch (Exception e) {
			return e.getMessage();
		}
		
		
		if (isWindows()) {
			return executeWindowsCommand(new String[] {"C:\\WINDOWS\\System32\\mstsc.exe", 
													   "-v", hostname + ":" + port});
			
		} else if (isLinux()) {
			return executeLinuxCommand(new String[] {"/usr/bin/remmina", 
													 "-n", "-s", hostname + ":" + port, "-t", "RDP"});			
						
		} else {
			return "Unsupported platform, requires Windows or Linux.";
		}
	}

}
