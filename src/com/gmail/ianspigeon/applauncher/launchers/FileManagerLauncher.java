package com.gmail.ianspigeon.applauncher.launchers;

import java.io.File;



public class FileManagerLauncher extends Launcher {
	
	
	public String launch(String path) {
		
		System.out.println("FileManagerLauncher call detected." );
		
		try {
			validateParameter("path", path, "^[\\w.\\-_/]+$");
		
		} catch (Exception e) {
			return e.getMessage();
		}
		
		
		if (isWindows()) {
			
			try {
				
				if (directoryExists(path)) {
					
					path = path.replace("/", "\\"); // Convert to windows path separators
					return executeWindowsCommand(new String[] {"C:\\Windows\\explorer.exe", path});
					
				} else {
					
		    		return "Directory not found: " + path;
				}
				
			} catch ( SecurityException e ) {
				
				return "ERROR_NOT_PRIVILEGED";
				
			}				
			
			
		} else if (isLinux()) {			
			
			return executeLinuxCommand(new String[] {"/usr/bin/nautilus", "smb:" + path});						
		
			
		} else {
			
			return "Unsupported platform, requires Windows or Linux.";
		}
		
	}

	private Boolean directoryExists(String path){
		
		final String fpath = path;
		
	    Boolean result = java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction<Boolean>(){
                public Boolean run() {
                	
    				File directory = new File( fpath );
    				if ( directory.exists() ) {
    					
    					return true;
    					
    				}
    				
    		        return false;
    				
                }
            }
        );	
	    
	    return result;
		
	}	
	
	
}
