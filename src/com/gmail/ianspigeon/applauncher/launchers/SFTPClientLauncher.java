package com.gmail.ianspigeon.applauncher.launchers;



public class SFTPClientLauncher extends Launcher {
	
	
	public String launch(String hostname, String port) {
		
		System.out.println("SFTPClientLauncher call detected." );
		
		try {
			validateParameter("hostname", hostname, "^[\\w.\\-_]+$");
			validateParameter("port", port, "^[\\d]+$");
		
		} catch (Exception e) {
			return e.getMessage();
		}		
		
		
		if (isWindows()) {
			return executeWindowsCommand(new String[] {"C:\\Program Files\\WinSCP\\WinSCP.exe", 
								  					   "sftp://\"" + hostname + ":" + port});

		} else if (isLinux()) {			 
			return executeLinuxCommand(new String[] {"/usr/bin/nautilus", 
													 "sftp://" + hostname + ":" + port});
	
		} else {
			return "Unsupported platform, requires Windows or Linux.";
		}		
		
	}

}
