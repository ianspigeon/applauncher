package com.gmail.ianspigeon.applauncher.launchers;


public class ESXClientLauncher extends Launcher {
	
	
	public String launch(String hostname, String vm) {
		
		System.out.println("ESXClientLauncher call detected." );
		
		try {
			validateParameter("hostname", hostname, "^[\\w.\\-_]+$");
			validateParameter("vm", vm, "^[\\w.\\-_\\(\\)&/]+$");
			
		} catch (Exception e) {
			return e.getMessage();
		}
		
		
		if (isWindows()) {
			return executeWindowsCommand(new String[] {"C:\\Program Files\\Common Files\\VMware\\VMware VMRC Plug-in\\Internet Explorer\\vmware-vmrc.exe", 
								  					   "-h", hostname, vm});
			
		} else if (isLinux()) {			 
			return executeLinuxCommand(new String[] {"/usr/bin/vmplayer", 
													 "-h", hostname, vm});						
			
		} else {
			return "Unsupported platform, requires Windows or Linux.";
		}			
		
	}

}
