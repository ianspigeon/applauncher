package com.gmail.ianspigeon.applauncher.launchers;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public abstract class Launcher {
	
	protected final String SUCCESS = "SUCCESS";
	protected final String ERROR_NOT_PRIVILEGED = "Insufficient privileges to launch command.";
	
	
	protected static boolean isWindows() {	 
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("windows") >= 0);
	}
	

	protected static boolean isLinux() {	 
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("linux") >= 0);
	}
	
	
	protected void validateParameter(String name, String value, String validationRegEx) throws Exception {
		if (value == null || value.length() == 0) {
			
			throw new Exception("Required parameter '" + name + "' missing.");
		
		} else {
			
			Pattern pattern = Pattern.compile(validationRegEx);
			Matcher matcher = pattern.matcher(value);
			if (! matcher.matches()) {
				throw new Exception("Required parameter '" + name + "' has invalid value.");				
			}
			
		}
	}

	
	protected String executeWindowsCommand(final String[] command){

		try {
			
			String result = java.security.AccessController.doPrivileged(
			    new java.security.PrivilegedAction<String>() {
			        public String run() {
						try {
							
							File location = new File(command[0]);
							if ( location.exists() ) {					
						
								System.out.println("Executing privileged command, \'" + Arrays.toString(command) + "\'." );
								Runtime.getRuntime().exec(command);
								return SUCCESS;
							
							} else {
							
								// Check x86 dir on 64bit systems if not found
								File x86Path = new File("C:\\Program Files (x86)\\");
								if ( x86Path.exists() && command[0].indexOf(x86Path.toString()) == 0) {
									
									String[] x86cmd = {};
									System.arraycopy(command, 0, x86cmd, 0, command.length);						
									x86cmd[0] = x86cmd[0].replace("C:\\Program Files\\", "C:\\Program Files (x86)\\");
									
									location = new File(command[0]);
									if ( location.exists() ) {
										
										System.out.println("Executing privileged command, \'" + Arrays.toString(x86cmd) + "\'." );
										Runtime.getRuntime().exec(command);
										return SUCCESS;
										
									}
							
								}
							
								return "Command not found, \'" + command[0] + "\'.";
							
							}
							
						} catch (IOException e) {
							
							e.printStackTrace();						
							return "Recieved IOException: " +  e.getMessage();
	
						}
			        }
			    }
			);
			
			return result;

		} catch ( SecurityException e ) {
			
			return ERROR_NOT_PRIVILEGED;
			
		}	
		
	}

	
	
	protected String executeLinuxCommand(final String[] command){

		try	{
		
			String result = java.security.AccessController.doPrivileged(
				new java.security.PrivilegedAction<String>() {
					public String run() {
						try {
						
							File location = new File(command[0]);
							if ( location.exists() ) {
						
								System.out.println("Executing privileged command, \'" + Arrays.toString(command) + "\'." );
							    Runtime runtime = Runtime.getRuntime();
							    runtime.exec(command);
							    return SUCCESS;
							
							} else {
							
								return "Command not found, \'" + command[0] + "\'.";
							
							}
							
						} catch (IOException e) {
						
							e.printStackTrace();
							return "Recieved IOException: " +  e.getMessage();
						}
					}
					
				}
				
			);
			
			return result;

		} catch ( SecurityException e ) {
			
			return ERROR_NOT_PRIVILEGED;
			
		}
	}
		
	
}
