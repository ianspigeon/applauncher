package com.gmail.ianspigeon.applauncher.launchers;



public class SSHClientLauncher extends Launcher {

	public String launch(String hostname, String port, String username) {
		
		
		System.out.println("SSHClientLauncher call detected." );
		
		try {
			validateParameter("hostname", hostname, "^[\\w.\\-_]+$");
			validateParameter("port", port, "^[\\d]+$");
			
		} catch (Exception e) {
			return e.getMessage();
		}
		
		
		if (isWindows()) {
			return executeWindowsCommand(new String[] {"C:\\Program Files\\PuTTY\\putty.exe", 
								  					   "-ssh", "-P", port, hostname, "-l", username});
			
		} else if (isLinux()) {			
			// Run ssh with gnome-terminal
			return executeLinuxCommand(new String[] {"/usr/bin/gnome-terminal", 
							    					 "-x", "bash", "-c", "ssh -o 'StrictHostKeyChecking no' -p " + 
							    					 port + " -l '" + username + "' " + hostname + "; read"});						
			
		} else {
			return "Unsupported platform, requires Windows or Linux.";
		}		
	
	}	
	
}
