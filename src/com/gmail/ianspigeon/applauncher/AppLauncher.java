package com.gmail.ianspigeon.applauncher;
import java.applet.Applet;

import com.gmail.ianspigeon.applauncher.launchers.ESXClientLauncher;
import com.gmail.ianspigeon.applauncher.launchers.FileManagerLauncher;
import com.gmail.ianspigeon.applauncher.launchers.RDesktopLauncher;
import com.gmail.ianspigeon.applauncher.launchers.SFTPClientLauncher;
import com.gmail.ianspigeon.applauncher.launchers.SSHClientLauncher;



public class AppLauncher extends Applet {
	
	private static final long serialVersionUID = -2847451135215021978L;

	
	public String getVersion() {
		
		Package p = this.getClass().getPackage();
		return p.getSpecificationVersion().replaceAll("\"", "");
		
	}
	

	public void init() {
		
		System.out.println("Launching appLauncher version: " + getVersion() + " ...");
		System.out.println("OS Name: "    + System.getProperty("os.name") );
		System.out.println("OS Version: " + System.getProperty("os.version") );
		System.out.println("JVM Arch: "   + System.getProperty("os.arch") );
		
	}
	

	public String launchESXClient(String hostname, String vm) {
		
		return new ESXClientLauncher().launch(hostname, vm);
		
	}

	
	public String launchSSH(String hostname, String port, String username) {
		
		return new SSHClientLauncher().launch(hostname, port, username);

	}
	
	
	public String launchRDesktop(String hostname, String port) {
		
		return new RDesktopLauncher().launch(hostname, port);

	}	

	
	public String launchSFTP(String hostname, String port) {
		
		return new SFTPClientLauncher().launch(hostname, port);

	}
	
	
	public String launchFileManager(String path) {
		
		return new FileManagerLauncher().launch(path);		
		
	}	
	
}
